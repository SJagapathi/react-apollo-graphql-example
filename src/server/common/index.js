import { generateToken, authenticateToken } from './jwt';
import { createPasswordHash, validatePassword } from './pw-bcrypt';

module.exports =  {
  generateToken:generateToken,
  authenticateToken:authenticateToken,
  createPasswordHash:createPasswordHash,
  validatePassword:  validatePassword
}
