import Users from '../../model/users.model';
import { generateToken, authenticateToken, createPasswordHash, validatePassword } from '../../common';
const userResolver = {
    Mutation: {
        createUser: async (root, params) => {
            var res = {};
            var total_users = await Users.find().count();
            var dbUser = await Users.findOne({email: params.email});
            if (dbUser) {
                res.token = null;
                throw new Error('User Already Exist');
            }

            var userData = new Users({
                email: params.email,
                password: createPasswordHash(params.password),
                userType: params.isAdmin? "admin":"endUser",
                id: total_users + 1
            });
            await userData.save(function (err, data) {
                if (err)
                    return err;

                res = data;
            });
            res.token = generateToken({email: params.email, userType: userData.userType}, '24h');
            return res;
        },
        loginUser: async (root, params) => {
            let res = {};
            await Users.findOne({email: params.email}, function (err, data) {
                if (err)
                    return err;
                if (data.password) {

                    let pass = data.password;
                    let validPass = validatePassword(data.password, params.password);

                    if (validPass) {
                        res.userId = data.id;
                        res.token = generateToken({email: params.email, userType: data.userType, userId:data._id}, '24h');
                    }
                    else{
                        res.userId = null;
                        res.token = null;
                    }
                }
            });
            return res;
        },
    }
};

export default userResolver;
