const Task = `
type Author {
    _id: ID,
    email:String
}
input inputAuthor {
    userId:String!
    name: String!
}

type todoType{
        _id:ID!,
        taskName:String!,
        Author:Author
    }
    
    input todoInputType{
        taskName:String,
        userId:String  
    }
    
  type Query {
    multipleTodos: [todoType]
    singleTodo(taskName:String!): [todoType]
  }
  
  type Mutation{
    addTask (
      newTodo: todoInputType!
    ): todoType
    
    updateTask(id:ID!,updatedTodo:todoInputType!):  todoType
    deleteTask(
         id: ID!
    ): todoType
    
  }
  
  type Subscription{
    addedTask: todoType
  }
  `;

export default Task;