import jwt from 'jsonwebtoken';
import Authorities from '../model/rolesAndResponsibilities';
import { getAuthorities } from '../seed/permissions';
import schema from '../schema';
function authorizeUser(req,res,next) {
    if(req.body.operationName === "loginUser" || req.body.operationName === "createUser"){
       next();
    }else{
      let token = req.headers.authorization;
      if(token){
  			jwt.verify(token, 'secret', function(err,decoded){
  				if(err){
            res.json({success:false, message:" token authentication failed"});
  				}else{
            let prePermissionObject = schema[req.body.operationName];
            if(typeof prePermissionObject !== 'undefined'){
              let isPermissionAllowed = getAuthorities(decoded.userType, prePermissionObject.moduleName, prePermissionObject.operation);
              if(isPermissionAllowed){
               next();
             }else{
               res.send({success: false, message: 'You are not authorized to perform this operation'});
              }
            }else{
              // TODO if prePermissionObject is undefined throw proper error
              next();
            }
  				}
  			});
  		}else{
  			res.json({success:false, message:'No token provided'});
  		}
    }
}

export default authorizeUser;
