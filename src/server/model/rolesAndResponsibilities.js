import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const authoritiesSchema = new Schema({
  userType: { type: String, required: true },
  operations: [{
    name: { type: String, required: true },
    isAllowed: { type: Boolean, default: false }
  }]
}, { collection: 'authorities', timestamp: true });

authoritiesSchema.statics.getAuthorityByUserType = function (userType) {
  return this.findOne({ userType: userType }, { operations: 1, _id: 0 }).exec();
};

const Authorities = mongoose.model('Authorities', authoritiesSchema);

// authoritiesSchema.static.createAuthorityForUserType = function (userType, operations) {
//   let authority = new Authorities({ userType, operations });
//   return authority.save();
// }

module.exports = Authorities;