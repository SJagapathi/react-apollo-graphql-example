function todoPermissionSet() {
    return [
        {
            "userType": "admin",
            "C": true,
            "R": true,
            "U": true,
            "D": true
        }, {
            "userType": "endUser",
            "C": false,
            "R": true,
            "U": true,
            "D": false
        }
    ];
}

module.exports = todoPermissionSet;
