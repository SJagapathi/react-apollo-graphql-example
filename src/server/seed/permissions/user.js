function userPermissionSet() {
    return [
        {
            "userType": "admin",
            "C": true,
            "R": true,
            "U": true,
            "D": true
        }, {
            "userType": "endUser",
            "C": true,
            "R": false,
            "U": true,
            "D": false
        }
    ];
}

module.exports = userPermissionSet;
