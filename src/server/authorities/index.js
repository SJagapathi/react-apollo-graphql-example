function getAuthorities(userType) {
	let userAuthority;
	try{
		userAuthority = require(`./${userType}`);
	}catch(ex) {
		if(ex.code === 'MODULE_NOT_FOUND') {
			throw new Error(`No authorities found for user type: ${userType}`);
		}
		throw ex;
	}
	return userAuthority;
}

module.exports = {
	authorities: getAuthorities
}
