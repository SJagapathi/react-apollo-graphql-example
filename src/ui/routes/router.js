import React from 'react';
import  ReactDOM from 'react-dom';
import App from '../components/App/App.js'
import LoginPage from '../components/Login/LoginPage.js'
import RegisterPage from '../components/AuthenticationForms/RegisterPage.js'
import InsertTask from '../components/tasks/insertTask.js'
import { Router,Route,Switch, Redirect} from 'react-router-dom';
import MainLayout from "../components/layout/layout.js";
import tasksGrid from "../components/tasksGrid/tasksGrid";
/*
const MatchWithMainLayout = ({ exactly, pattern, component: Component }: any) => {
    return (
        <Match exactly={exactly} pattern={pattern} render={(props: any) => (
            <MainLayout><Component {...props} /></MainLayout>
        )} />
    );
};*/

const PrivateRouter = ({ render: Component, ...rest }) => (
  <Route {...rest} render={props => (
    localStorage.getItem("token") ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/',
        state: { from: props.location }
      }}/>
    )
  )}/>
);

const Routes = (props) => (
    <Switch props={props}>
        <Route exact  path='/register' render={() =>(<RegisterPage {...props} />)} />
        <Route exact  path='/' render={() =>(<LoginPage />)} />
        <MainLayout>
            <PrivateRouter path="/home"  render={()=>(<App {...props} />)} />
            <PrivateRouter path="/addTask"  render={()=>(<InsertTask {...props} />)} />
            <PrivateRouter path="/tasksGrid" component={tasksGrid} />
        </MainLayout>
    </Switch>
);

export default Routes;
