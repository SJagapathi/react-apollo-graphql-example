import React, { Component } from 'react';
import Tasks from "../tasks/tasks.js";
import InsertTask from "../tasks/insertTask.js"


class App extends Component {
   constructor(props){
       super(props);
   }
   renderTasks(){
       return (
           <Tasks />
       )
   }

  render() {
    return (
        <div className="App-intro">
            {this.renderTasks()}
        </div>
    );
  }
}
/*const FeedQuery = gql`query multipleTodos {
  multipleTodos {
    _id
    taskName
  }
}`;
export default  graphql(FeedQuery)(App);*/

 export default App;

