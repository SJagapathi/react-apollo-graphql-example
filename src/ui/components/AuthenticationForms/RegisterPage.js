
import React, {Component} from 'react';
import RegisterForm from './RegisterForm';
import {gql,graphql} from 'react-apollo';
import toastr from 'toastr';
class RegisterPage extends Component{
    constructor(props){
        super(props)
        this.state = {
            errors: '',
            user: {},
            redirectTo:false
        }
        this.state.user.isAdmin = false;
        this.OnSubmit = this.OnSubmit.bind(this);
        this.OnChange = this.OnChange.bind(this);
        this.OnToggle = this.OnToggle.bind(this);
    }

    OnSubmit(event){
        event.preventDefault();
        console.log(this.state.user);
        const email = this.state.user.email;
        const password = this.state.user.password;
        const isAdmin = this.state.user.isAdmin;
        this.props.createUser(email,password,isAdmin)
            .then((data) => {

                if(data.error){
                    this.setState({user:{},errors: data.error});
                }else{
                    toastr.clear();
                    toastr.success("DONE");
                    this.setState({user:{},errors:"", redirectTo: true});
                }
            })
            .catch(err => {
                console.error("error");
                this.setState({errors: err});
            });

    }

    OnToggle(event, toggle) {
        const field = event.target.name;
        const user = this.state.user;
        user[field] = toggle;
        this.setState({user});
    }

    OnChange(event){
        const field = event.target.name;
        const user = this.state.user;
        user[field] = event.target.value;
        this.setState({user});
    }
    render(){
        return(
            <div className="App-intro">
            <RegisterForm
                OnSubmit={this.OnSubmit}
                OnChange={this.OnChange}
                OnToggle={this.OnToggle}
                errors={this.state.errors}
                user={this.state.user}
                redirectTo={this.state.redirectTo}
            />
                </div>
        );
    }
}


const addUserMutation = gql `
     mutation createUser($email: String!,$password: String!,$isAdmin: Boolean!){
         createUser(email:$email,password:$password, isAdmin:$isAdmin) {
             email
         }
     }
 `;

const addUserWithMutation = graphql(addUserMutation,{
    props:({mutate}) => ({
        createUser: (email,password, isAdmin) =>
            mutate({
                variables:{email,password,isAdmin}
            })
    })
})(RegisterPage);

export default addUserWithMutation;